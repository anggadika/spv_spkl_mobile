import 'package:flutter/material.dart';
import 'package:spv_spkl_mobile_clonning/models/worker_model.dart';

WorkerModel? userLogin;

BuildContext? ctxGlobal;

class MyAlert {
  snackBar(BuildContext context, String text) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(text),
        duration: const Duration(seconds: 2),
      ),
    );
  }

  popUp(BuildContext context, String text) => showDialog(
        context: context,
        builder: (BuildContext dialogContext) => AlertDialog(
            content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              const Text("Warning"),
              IconButton(
                onPressed: () => Navigator.pop(dialogContext),
                icon: const Icon(
                  Icons.clear,
                  color: Colors.red,
                ),
              ),
            ]),
            const SizedBox(
              height: 10,
            ),
            const Icon(
              Icons.warning,
              color: Colors.orange,
              size: 80,
            ),
            Text(
              text,
              style: const TextStyle(fontSize: 14, fontStyle: FontStyle.italic),
            ),
          ],
        )),
      );

  Future<String?> confirmPopUp(BuildContext context, String action) =>
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text('SPKL - $action'),
          content: const Text('Are you sure ?'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, 'OK'),
              child: const Text('OK'),
            ),
          ],
        ),
      );
}
