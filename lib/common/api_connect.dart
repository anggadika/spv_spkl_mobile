import 'package:http/http.dart' as http;
import 'package:spv_spkl_mobile_clonning/common/app_global.dart';

class ApiConnect {
  // final _baseUrl = 'http://172.30.10.5:10/api/Values';
  final _baseUrl = 'http://localhost:59597/api/values';
  // final _baseUrl = 'http://192.168.1.246:2201/api/Values';

  _setHeaders() =>
      {'Content-type': 'application/json', 'Accept': 'application/json'};

  Future<String> getApi(String endUrl) async {
    String _dataBody = "";
    final url = Uri.parse(_baseUrl + endUrl);

    final response = await http.get(url, headers: _setHeaders());

    if (response.statusCode == 200) {
      _dataBody = response.body;
    } else {
      MyAlert().snackBar(ctxGlobal!, 'API error, silahkan hubungi IT.');
    }

    return _dataBody;
  }

  Future<String> postApi(String endUrl, String rawBody) async {
    String _dataBody = "";
    final url = Uri.parse(_baseUrl + endUrl);

    final response =
        await http.post(url, headers: _setHeaders(), body: rawBody);

    if (response.statusCode == 200) {
      _dataBody = response.body;
    } else {
      MyAlert().snackBar(ctxGlobal!, 'API error, silahkan hubungi IT.');
    }

    return _dataBody;
  }
}
