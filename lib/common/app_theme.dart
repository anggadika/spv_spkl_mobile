import 'package:flutter/material.dart';

final appTheme = ThemeData(
  primarySwatch: Colors.red,
  appBarTheme:
      const AppBarTheme(backgroundColor: Color.fromRGBO(37, 46, 45, 1)),
  scaffoldBackgroundColor: Colors.grey[100],
  indicatorColor: Colors.yellow,
  snackBarTheme: snackBarTheme,
  tabBarTheme: tabBarTheme,
  iconTheme: const IconThemeData(color: Colors.grey),
);

final snackBarTheme = SnackBarThemeData(
  backgroundColor: Colors.red[800],
  actionTextColor: Colors.white,
  disabledActionTextColor: Colors.grey,
  shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(10))),
  behavior: SnackBarBehavior.floating,
);

final tabBarTheme = TabBarTheme(
    labelColor: Colors.orangeAccent, unselectedLabelColor: Colors.grey[200]);
