import 'package:flutter/material.dart';
import 'package:spv_spkl_mobile_clonning/pages/home_page.dart';
import 'package:spv_spkl_mobile_clonning/pages/init_page.dart';

class AppRoute {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case "/":
        return MaterialPageRoute(builder: (_) => const InitPage());
      case "/home":
        return MaterialPageRoute(builder: (_) => const HomePage());
      default:
        return MaterialPageRoute(
          builder: (_) {
            return Scaffold(
              body: Center(
                child: Text("No route defined for ${settings.name}"),
              ),
            );
          },
        );
    }
  }
}
