import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spv_spkl_mobile_clonning/common/app_global.dart';
import 'package:spv_spkl_mobile_clonning/providers/auth_provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final _usernameTEC = TextEditingController();
  final _passwordTEC = TextEditingController();

  @override
  Widget build(BuildContext context) {
    ctxGlobal = context;
    final authState = Provider.of<AuthProvider>(context);
    // _usernameTEC.text = 'albert@gio.com';
    // _passwordTEC.text = '123456';

    return Scaffold(
      body: SafeArea(
          child: Center(
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0, right: 10.0),
          child: Stack(
            fit: StackFit.expand,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SingleChildScrollView(
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(maxWidth: 500),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Image.asset(
                              "assets/images/logo_spv.png",
                              fit: BoxFit.fitWidth,
                              width: 300,
                            ),
                            const SizedBox(height: 30),
                            TextFormField(
                              controller: _usernameTEC,
                              validator: validateEmpty,
                              decoration: const InputDecoration(
                                filled: true,
                                fillColor: Color.fromRGBO(247, 242, 242, 1),
                                prefixIcon: Icon(Icons.person),
                                labelText: "Username",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20.0),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 10),
                            TextFormField(
                              controller: _passwordTEC,
                              validator: validateEmpty,
                              obscureText: true,
                              decoration: const InputDecoration(
                                filled: true,
                                fillColor: Color.fromRGBO(247, 242, 242, 1),
                                prefixIcon: Icon(Icons.key),
                                labelText: "Password",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20.0),
                                  ),
                                ),
                              ),
                              onFieldSubmitted: (value) async {
                                if (_formKey.currentState!.validate()) {
                                  String checkLogin = await authState.login(
                                      _usernameTEC.text, _passwordTEC.text);

                                  if (checkLogin != 'success') {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(content: Text(checkLogin)),
                                    );
                                  } else {
                                    Navigator.pushNamed(context, '/home');
                                  }
                                }
                              },
                            ),
                            const SizedBox(
                              height: 30,
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: Colors.red,
                                  onPrimary: Colors.white,
                                  textStyle: const TextStyle(fontSize: 16),
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 40, vertical: 20),
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(30.0))),
                              onPressed: () async {
                                if (_formKey.currentState!.validate()) {
                                  String checkLogin = await authState.login(
                                      _usernameTEC.text, _passwordTEC.text);

                                  if (checkLogin != 'success') {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(content: Text(checkLogin)),
                                    );
                                  } else {
                                    Navigator.pushNamed(context, '/home');
                                  }
                                }
                              },
                              child: const Icon(Icons.login),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
              if (authState.loadingState)
                const Center(child: CircularProgressIndicator())
            ],
          ),
        ),
      )),
      persistentFooterButtons: [
        Center(
          child: Text("SPKL Mobile | SPVMB © ${DateTime.now().year}",
              style:
                  const TextStyle(fontSize: 12, fontStyle: FontStyle.italic)),
        ),
      ],
    );
  }

  String? validateEmpty(String? value) {
    if (value == null || value.isEmpty) {
      return 'Please enter some text';
    }
    return null;
  }
}
