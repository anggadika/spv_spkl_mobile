import 'dart:html';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spv_spkl_mobile_clonning/main.dart';
import 'package:spv_spkl_mobile_clonning/models/head_model.dart';
import 'package:spv_spkl_mobile_clonning/pages/detail_page.dart';
import 'package:spv_spkl_mobile_clonning/pages/widgets/my_app_bar.dart';
import 'package:spv_spkl_mobile_clonning/pages/widgets/spkl_dialog.dart';
import 'package:spv_spkl_mobile_clonning/providers/head_provider.dart';

import '../common/app_global.dart';
import 'widgets/my_table.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _searchTEC = TextEditingController();
  HeadProvider _headProvider = HeadProvider();
  @override
  Widget build(BuildContext context) {
    _headProvider = Provider.of<HeadProvider>(context, listen: false);
    final _widthLength = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Center(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 1200),
          child: Container(
            decoration: const BoxDecoration(
              border: Border(
                left: BorderSide(color: Color(0xFFEEEEEE), width: 2.0),
                right: BorderSide(color: Color(0xFFEEEEEE), width: 2.0),
              ),
            ),
            child: FutureBuilder(
              future: _headProvider.loadData(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(child: CircularProgressIndicator());
                }
                return Consumer<HeadProvider>(
                  builder: (context, dataConsum, _) {
                    return Column(
                      children: [
                        const MyAppBar(),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    padding: const EdgeInsets.only(right: 10),
                                    child: TextField(
                                      controller: _searchTEC,
                                      onSubmitted: (valube) => _headProvider
                                          .searchData(_searchTEC.text),
                                      decoration: InputDecoration(
                                        suffixIcon: IconButton(
                                          onPressed: () => HeadProvider()
                                              .searchData(_searchTEC.text),
                                          icon: const Icon(Icons.search),
                                          color: Colors.green,
                                        ),
                                        filled: true,
                                        fillColor: Colors.grey,
                                        hintText: "SPKL Cari",
                                        border: const OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        isDense: true,
                                        contentPadding:
                                            const EdgeInsets.fromLTRB(
                                                10, 10, 10, 0),
                                      ),
                                    ),
                                  ),
                                  ElevatedButton(
                                    onPressed: () =>
                                        SpklDialog().action(context),
                                    style: ElevatedButton.styleFrom(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 15),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                    ),
                                    child: _widthLength > 480
                                        ? const Text("+ SPKL +")
                                        : const Icon(Icons.add),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        if (dataConsum.loadingState)
                          const LinearProgressIndicator(),
                        Flexible(
                          child: _widthLength > 480
                              ? SingleChildScrollView(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        border: Border.all(color: Colors.brown),
                                      ),
                                      child: Table(
                                        border: const TableBorder(
                                            horizontalInside: BorderSide(
                                                color: Colors.orange,
                                                style: BorderStyle.solid)),
                                        children: [
                                          TableRow(
                                            children: [
                                              MyTable().th('Nomor'),
                                              MyTable().th('Tanggal'),
                                              MyTable().th('Keterangan'),
                                              MyTable().th('Status'),
                                              MyTable().th(''),
                                            ],
                                          ),
                                          for (var item in dataConsum.dataList)
                                            TableRow(
                                              children: [
                                                MyTable()
                                                    .td(item.recId.toString()),
                                                MyTable().td(item.transDate),
                                                MyTable().td(item.description),
                                                MyTable()
                                                    .td(item.approvalStatus),
                                                Row(
                                                  children: [
                                                    IconButton(
                                                      onPressed: () =>
                                                          Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                              DetailPage(
                                                                  headModel:
                                                                      item),
                                                        ),
                                                      ),
                                                      icon: const Icon(Icons
                                                          .open_in_new_rounded),
                                                      tooltip: "Details",
                                                      color: Colors.red,
                                                    ),
                                                    IconButton(
                                                      onPressed:
                                                          item.approvalStatus ==
                                                                      "Draft" ||
                                                                  item.approvalStatus ==
                                                                      "Rejected"
                                                              ? () async {
                                                                  MyAlert()
                                                                      .confirmPopUp(
                                                                          context,
                                                                          "Delete")
                                                                      .then(
                                                                    (value) async {
                                                                      if (value ==
                                                                          "OK") {
                                                                        await _headProvider.action(
                                                                            "/DeleteSPKL",
                                                                            item.recId);
                                                                      }
                                                                    },
                                                                  );
                                                                }
                                                              : null,
                                                      icon: const Icon(
                                                          Icons.delete),
                                                      tooltip: "Delete",
                                                      color: Colors.green,
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              : ListView.builder(
                                  itemBuilder: (context, index) =>
                                      _dataItem(dataConsum.dataList[index]),
                                  itemCount: dataConsum.dataList.length,
                                ),
                        ),
                      ],
                    );
                  },
                );
              },
            ),
          ),
        ),
      ),
      persistentFooterButtons: [
        Center(
          child: Text(
            "SPKL Mobile | SPVMB © ${DateTime.now().year}",
            style: const TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
          ),
        ),
      ],
    );
  }

  _dataItem(HeadModel item) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.topLeft,
                colors: [Colors.yellow, Colors.green],
              ),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5),
                topRight: Radius.circular(5),
              ),
            ),
            padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SelectableText(
                      "${item.recId}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.brown),
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    Text(
                      item.transDate ?? "",
                      style: const TextStyle(color: Colors.pink),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: SelectableText(item.description ?? ""),
                ),
                const SizedBox(
                  width: 8,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    IconButton(
                      onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DetailPage(headModel: item),
                        ),
                      ),
                      tooltip: "Details",
                      color: Colors.brown,
                      icon: const Icon(Icons.open_in_new_rounded),
                    ),
                    IconButton(
                      onPressed: (item.approvalStatus == "Draft" ||
                              item.approvalStatus == "Rejected"
                          ? () async {
                              MyAlert()
                                  .confirmPopUp(context, "Delete")
                                  .then((value) async {
                                await _headProvider.action(
                                    "/DeleteSPKL", item.recId);
                              });
                            }
                          : null),
                      icon: const Icon(Icons.delete),
                      tooltip: "Delete",
                      color: Colors.yellow,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
