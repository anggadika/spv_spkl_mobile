import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import 'package:spv_spkl_mobile_clonning/models/head_model.dart';
import 'package:spv_spkl_mobile_clonning/models/line_model.dart';
import 'package:spv_spkl_mobile_clonning/pages/widgets/line_dialog.dart';
import 'package:spv_spkl_mobile_clonning/pages/widgets/spkl_dialog.dart';
import 'package:spv_spkl_mobile_clonning/providers/head_provider.dart';
import '../common/app_global.dart';
import '../providers/line_provider.dart';
import 'widgets/my_table.dart';

class DetailPage extends StatefulWidget {
  final HeadModel headModel;
  const DetailPage({Key? key, required this.headModel}) : super(key: key);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  LineProvider _lineProvider = LineProvider();
  @override
  Widget build(BuildContext context) {
    final _widthLength = MediaQuery.of(context).size.width;
    _lineProvider = Provider.of<LineProvider>(context, listen: false);
    final _headState = Provider.of<HeadProvider>(context, listen: false);
    _lineProvider.headData = widget.headModel;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "${widget.headModel.recId} ${widget.headModel.approvalStatus}",
        ),
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.grey, Colors.blueGrey],
            ),
          ),
        ),
      ),
      body: Center(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 1200),
          child: Container(
            decoration: const BoxDecoration(
              border: Border(
                left: BorderSide(color: Color(0xFFEEEEEE), width: 2.0),
                right: BorderSide(color: Color(0xFFEEEEEE), width: 2.0),
              ),
            ),
            child: FutureBuilder(
              future: _lineProvider.fetchData(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return Consumer<LineProvider>(
                  builder: (context, dataConsum, _) {
                    return Column(
                      children: [
                        Card(
                          margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        const Text(
                                          "SPKL Info",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        IconButton(
                                            onPressed: widget.headModel
                                                        .approvalStatus ==
                                                    "Draft"
                                                ? () {
                                                    SpklDialog().action(context,
                                                        dataLast:
                                                            widget.headModel,
                                                        adaLine: dataConsum
                                                                .dataList
                                                                .isNotEmpty
                                                            ? true
                                                            : false);
                                                  }
                                                : null,
                                            icon: const Icon(Icons.edit),
                                            color: Colors.red),
                                      ],
                                    ),
                                    ElevatedButton(
                                      onPressed: (widget.headModel
                                                          .approvalStatus ==
                                                      "Draft" ||
                                                  widget.headModel
                                                          .approvalStatus ==
                                                      "Rejected") &&
                                              dataConsum.dataList.isNotEmpty
                                          ? () => MyAlert()
                                                  .confirmPopUp(
                                                      context, "Submit")
                                                  .then((value) async {
                                                if (value == "OK") {
                                                  await _headState.action(
                                                      "/SubmitSPKL",
                                                      widget.headModel.recId);
                                                  Navigator.pop(context);
                                                }
                                              })
                                          : null,
                                      style: ElevatedButton.styleFrom(
                                        primary: Colors.green,
                                        onPrimary: Colors.white,
                                        textStyle:
                                            const TextStyle(fontSize: 16),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                        ),
                                      ),
                                      child: const Text("Submit"),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                _listInfo(
                                    "Tanggal", widget.headModel.transDate),
                                _listInfo("LOB", widget.headModel.lob),
                                _listInfo(
                                    "Department", widget.headModel.departement),
                                _listInfo("Alasan Lembur",
                                    widget.headModel.description),
                              ],
                            ),
                          ),
                        ),
                        Flexible(
                          child: Card(
                            margin: const EdgeInsets.only(left: 10, right: 10),
                            child: Padding(
                              padding: const EdgeInsets.all(8),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      const Text(
                                        "Worker List",
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      ElevatedButton(
                                        onPressed: (widget.headModel
                                                        .approvalStatus ==
                                                    "Draft" ||
                                                widget.headModel
                                                        .approvalStatus ==
                                                    "Rejected")
                                            ? () => LineDialog().action(context)
                                            : null,
                                        style: ElevatedButton.styleFrom(
                                          primary: Colors.red,
                                          onPrimary: Colors.white,
                                          textStyle:
                                              const TextStyle(fontSize: 16),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(30),
                                          ),
                                        ),
                                        child: const Icon(Icons.person_add),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  if (dataConsum.loadingState)
                                    const LinearProgressIndicator(),
                                  dataConsum.dataList.isEmpty
                                      ? const Center(
                                          child: Text(
                                            "Belum ada Worker...",
                                            style: TextStyle(
                                                fontStyle: FontStyle.italic,
                                                color: Colors.grey),
                                          ),
                                        )
                                      : Flexible(
                                          child: _widthLength > 700
                                              ? SingleChildScrollView(
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              8),
                                                      border: Border.all(
                                                          color: Colors.green),
                                                    ),
                                                    child: Table(
                                                      columnWidths: const {
                                                        0: FlexColumnWidth(1),
                                                        1: FlexColumnWidth(3),
                                                        2: FlexColumnWidth(2),
                                                        3: FlexColumnWidth(2),
                                                        4: FlexColumnWidth(2),
                                                        5: FlexColumnWidth(3),
                                                        6: FlexColumnWidth(2),
                                                        7: FlexColumnWidth(2),
                                                        8: FlexColumnWidth(1),
                                                        9: FlexColumnWidth(1),
                                                      },
                                                      border: const TableBorder(
                                                        horizontalInside:
                                                            BorderSide(
                                                          color: Colors.yellow,
                                                          style:
                                                              BorderStyle.solid,
                                                        ),
                                                        bottom: BorderSide(
                                                            color: Colors.pink),
                                                      ),
                                                      children: [
                                                        TableRow(children: [
                                                          MyTable().th("No"),
                                                          MyTable().th("Nama"),
                                                          MyTable().th("Mulai"),
                                                          MyTable()
                                                              .th("Selesai"),
                                                          MyTable()
                                                              .th("Jumlah"),
                                                          MyTable()
                                                              .th("Uraian"),
                                                          MyTable()
                                                              .th("Target"),
                                                          MyTable().th("Hasil"),
                                                          MyTable()
                                                              .th('Diawal'),
                                                          MyTable().th(""),
                                                          MyTable().th(""),
                                                        ]),
                                                        for (var item = 0;
                                                            item <
                                                                dataConsum
                                                                    .dataList
                                                                    .length;
                                                            item++)
                                                          TableRow(
                                                            children: [
                                                              MyTable().td(
                                                                  "${item + 1}"),
                                                              MyTable().td(
                                                                  dataConsum
                                                                      .dataList[
                                                                          item]
                                                                      .workerName),
                                                              MyTable().td(
                                                                  dataConsum
                                                                      .dataList[
                                                                          item]
                                                                      .fromTime
                                                                      .toString()
                                                                      .substring(
                                                                          0,
                                                                          5)),
                                                              MyTable().td(
                                                                  dataConsum
                                                                      .dataList[
                                                                          item]
                                                                      .toTime
                                                                      .toString()
                                                                      .substring(
                                                                          0,
                                                                          5)),
                                                              MyTable().td(dataConsum
                                                                  .dataList[
                                                                      item]
                                                                  .totalJamLembur
                                                                  .toString()
                                                                  .substring(
                                                                      0, 4)),
                                                              MyTable().td(
                                                                  dataConsum
                                                                      .dataList[
                                                                          item]
                                                                      .note),
                                                              MyTable().td(
                                                                  dataConsum
                                                                      .dataList[
                                                                          item]
                                                                      .targetLembur),
                                                              MyTable().td(
                                                                  dataConsum
                                                                      .dataList[
                                                                          item]
                                                                      .hasilLembur),
                                                              Align(
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                child: Checkbox(
                                                                  value: dataConsum
                                                                              .dataList[item]
                                                                              .diAwal ==
                                                                          1
                                                                      ? true
                                                                      : false,
                                                                  onChanged:
                                                                      null,
                                                                ),
                                                              ),
                                                              Row(
                                                                children: [
                                                                  IconButton(
                                                                    onPressed: (widget.headModel.approvalStatus ==
                                                                                "Draft" ||
                                                                            widget.headModel.approvalStatus ==
                                                                                "Rejected")
                                                                        ? () {
                                                                            LineDialog().action(context,
                                                                                dataLast: dataConsum.dataList[item]);
                                                                          }
                                                                        : null,
                                                                    icon: const Icon(
                                                                        Icons
                                                                            .edit),
                                                                    tooltip:
                                                                        "Edit",
                                                                    color: Colors
                                                                        .orange,
                                                                  )
                                                                ],
                                                              ),
                                                              Row(
                                                                children: [
                                                                  IconButton(
                                                                    onPressed: (widget.headModel.approvalStatus ==
                                                                                'Draft' ||
                                                                            widget.headModel.approvalStatus ==
                                                                                'Rejected')
                                                                        ? () async => await _lineProvider.delete(dataConsum
                                                                            .dataList[item]
                                                                            .recId)
                                                                        : null,
                                                                    tooltip:
                                                                        "Delete",
                                                                    color: Colors
                                                                        .orange,
                                                                    icon: const Icon(
                                                                        Icons
                                                                            .delete),
                                                                  )
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              : ListView.builder(
                                                  itemBuilder: (context,
                                                          index) =>
                                                      _dataItem(
                                                          context,
                                                          index,
                                                          dataConsum
                                                              .dataList[index]),
                                                ),
                                        ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                );
              },
            ),
          ),
        ),
      ),
      persistentFooterButtons: [
        Center(
          child: Text("SPKL Mobile | SPVMB © ${DateTime.now().year}",
              style:
                  const TextStyle(fontSize: 12, fontStyle: FontStyle.italic)),
        ),
      ],
    );
  }

  _listInfo(String label, String? value) => Padding(
        padding: const EdgeInsets.only(left: 8, bottom: 8),
        child: Row(
          children: [
            SizedBox(
              width: 120,
              child: Text("$label :"),
            ),
            Flexible(
              child: Text(value!),
            ),
          ],
        ),
      );

  _dataItem(BuildContext context, int count, LineModel item) {
    return Card(
      shadowColor: Colors.yellow,
      elevation: 10,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(color: Colors.blue, width: 1),
              ),
            ),
            padding: const EdgeInsets.fromLTRB(10, 4, 10, 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("${(count + 1).toString()}."),
                const SizedBox(
                  width: 10,
                ),
                Expanded(child: _iconText(Icons.person_sharp, item.workerName)),
                IconButton(
                  onPressed: (widget.headModel.approvalStatus == "Draft" ||
                          widget.headModel.approvalStatus == "Rejected")
                      ? () => LineDialog().action(context, dataLast: item)
                      : null,
                  tooltip: "Edit",
                  color: Colors.green,
                  icon: const Icon(Icons.edit),
                ),
                IconButton(
                  onPressed: (widget.headModel.approvalStatus == "Draft" ||
                          widget.headModel.approvalStatus == "Rejected")
                      ? () async => await _lineProvider.delete(item.recId)
                      : null,
                  tooltip: "dELETE",
                  color: Colors.yellow,
                  icon: const Icon(Icons.delete),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Padding(padding: EdgeInsets.fromLTRB(10, 20, 0, 70)),
              Expanded(
                child: Column(
                  children: [
                    _iconText(
                        (item.diAwal == 1
                            ? Icons.radio_button_checked
                            : Icons.radio_button_unchecked),
                        "diawal"),
                    const SizedBox(
                      height: 3,
                    ),
                    _iconText(Icons.add_alarm_rounded,
                        item.totalJamLembur.toString().substring(0, 4)),
                    const SizedBox(
                      height: 3,
                    ),
                    _iconText(Icons.description, item.note),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    _iconText(Icons.alarm_on,
                        "${item.fromTime.toString().substring(0, 5)} - ${item.toTime.toString().substring(0, 5)}"),
                    const SizedBox(
                      height: 3,
                    ),
                    _iconText(Icons.checklist_outlined,
                        "${item.targetLembur} - ${item.hasilLembur}"),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  _iconText(IconData? icon, String? value) => Row(
        children: [
          Icon(
            icon,
            color: Colors.blueGrey,
          ),
          const SizedBox(
            width: 5,
          ),
          Flexible(child: Text(value ?? ''))
        ],
      );
}
