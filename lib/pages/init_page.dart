import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spv_spkl_mobile_clonning/common/app_global.dart';
import 'package:spv_spkl_mobile_clonning/providers/auth_provider.dart';

import 'home_page.dart';
import 'login_page.dart';

class InitPage extends StatelessWidget {
  const InitPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ctxGlobal = context;
    return FutureBuilder(
      future: Provider.of<AuthProvider>(context, listen: false).checkLogin(),
      builder: (context, snapshot) {
        return snapshot.hasData ? const HomePage() : const LoginPage();
      },
    );
  }
}
