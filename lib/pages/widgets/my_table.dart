import 'package:flutter/material.dart';

class MyTable {
  Widget th(String label, {bool right = false}) => Padding(
        padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
        child: Text(
          label,
          textAlign: right ? TextAlign.right : TextAlign.left,
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
      );

  Widget td(String? value, {bool right = false}) => Padding(
      padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
      child: Text(value ?? '',
          textAlign: right ? TextAlign.right : TextAlign.left));
}
