import 'package:dropdown_search2/dropdown_search2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spv_spkl_mobile_clonning/common/app_global.dart';
import 'package:spv_spkl_mobile_clonning/models/line_model.dart';
import 'package:spv_spkl_mobile_clonning/models/worker_model.dart';
import 'package:spv_spkl_mobile_clonning/providers/auth_provider.dart';
import 'package:spv_spkl_mobile_clonning/providers/line_provider.dart';

class LineDialog {
  action(BuildContext context, {LineModel? dataLast, bool adaLine = false}) {
    final _lineState = Provider.of<LineProvider>(context, listen: false);
    final _authState = Provider.of<AuthProvider>(context, listen: false);
    final _formKey = GlobalKey<FormState>();
    final _mulaiTEC = TextEditingController();
    final _selesaiTEC = TextEditingController();
    final _uraianTEC = TextEditingController();
    final _targetTEC = TextEditingController();
    final _hasilTEC = TextEditingController();
    TimeOfDay? selectedTime;

    if (dataLast != null) {
      _mulaiTEC.text = dataLast.fromTime!.substring(0, 5);
      _selesaiTEC.text = dataLast.toTime!.substring(0, 5);
      _uraianTEC.text = dataLast.note!;
      _targetTEC.text = dataLast.targetLembur!;
      _hasilTEC.text = dataLast.hasilLembur!;
    }

    _authState.fetchData();

    _timePicker() async => await showTimePicker(
          context: context,
          initialTime: selectedTime ?? const TimeOfDay(hour: 17, minute: 00),
          initialEntryMode: TimePickerEntryMode.dial,
          builder: (BuildContext context, Widget? child) {
            return MediaQuery(
              data:
                  MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
              child: child!,
            );
          },
        );

    return showDialog(
        context: context,
        useRootNavigator: false,
        builder: (context) {
          return Dialog(
            insetPadding: const EdgeInsets.all(20),
            child: Consumer<LineProvider>(builder: (context, dataConsum, _) {
              return SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: ConstrainedBox(
                    constraints: const BoxConstraints(maxWidth: 600),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        ListTile(
                          title: Text(
                              dataLast == null ? "Add worker" : "Edit Worker"),
                          trailing: IconButton(
                            onPressed: () => Navigator.pop(context),
                            icon: const Icon(
                              Icons.clear,
                              color: Colors.red,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              FutureBuilder(
                                  future: _authState.fetchData(),
                                  builder: (context, snapshot) {
                                    if (snapshot.connectionState ==
                                        ConnectionState.waiting) {
                                      return const Center(
                                          child: CircularProgressIndicator());
                                    }
                                    return DropdownSearch<WorkerModel>(
                                      items: _authState.dataList,
                                      showClearButton: true,
                                      itemAsString: (WorkerModel? u) =>
                                          "${u?.name} - ${u?.personnelNumber}",
                                      maxHeight: 300,
                                      // selectedItem: _lineState.workerPick,
                                      selectedItem: dataLast == null
                                          ? null
                                          : _lineState.workerPick = _authState
                                              .dataList
                                              .singleWhere((element) =>
                                                  element.departement ==
                                                      dataLast.departement &&
                                                  element.recId ==
                                                      dataLast.workerId),
                                      dropdownSearchDecoration:
                                          const InputDecoration(
                                        labelText: "pilih worker",
                                        contentPadding:
                                            EdgeInsets.fromLTRB(12, 12, 0, 0),
                                        border: OutlineInputBorder(),
                                      ),
                                      onChanged: (WorkerModel? u) {
                                        _lineState.workerPick = u!;
                                      },
                                      showSearchBox: true,
                                    );
                                  }),
                              const Divider(),
                              Row(
                                children: [
                                  Flexible(
                                    child: TextFormField(
                                      controller: _mulaiTEC,
                                      validator: validateEmpty,
                                      readOnly: true,
                                      onTap: () async {
                                        selectedTime = await _timePicker();

                                        if (selectedTime != null) {
                                          _mulaiTEC.text =
                                              "${selectedTime!.hour.toString().padLeft(2, "0")}:${selectedTime!.minute > 0 ? '30' : '00'}";
                                        }
                                      },
                                      decoration: const InputDecoration(
                                        labelText: "Jam mulai",
                                        border: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  const Icon(Icons.remove),
                                  Flexible(
                                    child: TextFormField(
                                      controller: _selesaiTEC,
                                      validator: validateEmpty,
                                      readOnly: true,
                                      onTap: () async {
                                        selectedTime = await _timePicker();

                                        if (selectedTime != null) {
                                          _selesaiTEC.text =
                                              "${selectedTime!.hour.toString().padLeft(2, "0")}:${selectedTime!.minute > 0 ? '30' : '00'}";
                                        }
                                      },
                                      decoration: const InputDecoration(
                                        labelText: "Jam selesai",
                                        border: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              TextFormField(
                                controller: _uraianTEC,
                                validator: validateEmpty,
                                textInputAction: TextInputAction.next,
                                decoration: const InputDecoration(
                                  labelText: "Uraian lembur",
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10.0),
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Row(
                                children: [
                                  Flexible(
                                    child: TextFormField(
                                      controller: _targetTEC,
                                      validator: validateEmpty,
                                      textInputAction: TextInputAction.next,
                                      decoration: const InputDecoration(
                                        labelText: "Target lembur",
                                        border: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Flexible(
                                    child: TextFormField(
                                      controller: _hasilTEC,
                                      validator: validateEmpty,
                                      textInputAction: TextInputAction.done,
                                      decoration: const InputDecoration(
                                        labelText: "Hasil lembur",
                                        border: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 15.0),
                              dataConsum.loadingState
                                  ? const LinearProgressIndicator()
                                  : SizedBox(
                                      width: double.infinity,
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            primary: Colors.red,
                                            onPrimary: Colors.white,
                                            padding: const EdgeInsets.all(20),
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        10.0))),
                                        onPressed: () async {
                                          if (_formKey.currentState!
                                              .validate()) {
                                            if (dataLast == null) {
                                              final result =
                                                  await _lineState.added(
                                                _mulaiTEC.text,
                                                _selesaiTEC.text,
                                                _uraianTEC.text,
                                                _targetTEC.text,
                                                _hasilTEC.text,
                                              );
                                              if (result != "success") {
                                                MyAlert()
                                                    .popUp(context, result);
                                              } else {
                                                await _lineState.fetchData();
                                                Navigator.pop(context);
                                              }
                                            } else {
                                              final result =
                                                  await _lineState.edited(
                                                      _mulaiTEC.text,
                                                      _selesaiTEC.text,
                                                      _uraianTEC.text,
                                                      _targetTEC.text,
                                                      _hasilTEC.text);
                                              if (result != "success") {
                                                MyAlert()
                                                    .popUp(context, result);
                                              } else {
                                                await _lineState.fetchData();
                                                Navigator.pop(context);
                                              }
                                            }
                                          }
                                        },
                                        child: const Text("Simpan"),
                                      ),
                                    )
                            ],
                          ),
                        ),
                      ]),
                    ),
                  ),
                ),
              );
            }),
          );
        });
  }

  String? validateEmpty(String? value) {
    if (value == null || value.isEmpty) {
      return 'Harus di isi';
    }
    return null;
  }
}
