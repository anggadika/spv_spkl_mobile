import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:spv_spkl_mobile_clonning/models/head_model.dart';
import 'package:spv_spkl_mobile_clonning/providers/head_provider.dart';
import 'package:spv_spkl_mobile_clonning/providers/line_provider.dart';

class SpklDialog {
  action(BuildContext context, {HeadModel? dataLast, bool adaLine = false}) {
    final _headState = Provider.of<HeadProvider>(context, listen: false);
    final _lineState = Provider.of<LineProvider>(context, listen: false);
    final _formKey = GlobalKey<FormState>();
    final _dateCTE = TextEditingController();
    final _descCTE = TextEditingController();
    DateTime? pickedDate;

    if (dataLast != null) {
      _dateCTE.text = dataLast.transDate!;
      _descCTE.text = dataLast.description!;

      List lastDate = _dateCTE.text.split('/');
      pickedDate = DateTime(int.parse(lastDate[2]), int.parse(lastDate[1]),
          int.parse(lastDate[0]));
    }

    _datePicker() async => await showDatePicker(
        context: context,
        initialDate: pickedDate ?? DateTime.now(),
        firstDate: DateTime(2010),
        lastDate: DateTime(
            DateTime.now().year + 1, DateTime.now().month, DateTime.now().day));

    return showDialog(
        context: context,
        builder: (context) {
          return Form(
            key: _formKey,
            child: AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(dataLast != null ? "Update" : "New SPKL"),
                  IconButton(
                    onPressed: () => Navigator.pop(context),
                    icon: const Icon(
                      Icons.clear,
                      color: Colors.red,
                    ),
                  )
                ],
              ),
              content: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 800),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextFormField(
                        controller: _dateCTE,
                        validator: validateEmpty,
                        readOnly: true,
                        onTap: adaLine
                            ? null
                            : () async {
                                pickedDate = await _datePicker();

                                if (pickedDate != null) {
                                  _dateCTE.text = DateFormat("dd/MM/yyyy")
                                      .format(pickedDate!);
                                }
                              },
                        decoration: const InputDecoration(
                          labelText: "Tanggal lembur",
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      TextFormField(
                        controller: _descCTE,
                        validator: validateEmpty,
                        maxLines: 3,
                        decoration: const InputDecoration(
                          labelText: "Alasan mengajukan lembur",
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                          ),
                        ),
                        textInputAction: TextInputAction.done,
                      ),
                      const SizedBox(height: 15.0),
                      SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.red,
                              onPrimary: Colors.white,
                              padding: const EdgeInsets.all(20),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0))),
                          onPressed: () async {
                            if (_formKey.currentState!.validate()) {
                              if (dataLast != null) {
                                final result = await _headState.update(
                                    dataLast, _dateCTE.text, _descCTE.text);
                                _lineState.setHeadData(result);
                              } else {
                                final result = await _headState.added(
                                    _dateCTE.text, _descCTE.text);
                                if (result != "success") {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(content: Text(result)),
                                  );
                                } else {
                                  await _headState.loadData();
                                }
                              }

                              Navigator.pop(context);
                            }
                          },
                          child: const Text("Simpan"),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  String? validateEmpty(String? value) {
    if (value == null || value.isEmpty) {
      return 'Harus di isi';
    }
    return null;
  }
}
