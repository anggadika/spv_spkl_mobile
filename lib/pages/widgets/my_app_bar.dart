import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spv_spkl_mobile_clonning/common/app_global.dart';
import 'package:spv_spkl_mobile_clonning/providers/auth_provider.dart';

class MyAppBar extends StatelessWidget {
  const MyAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _authState = Provider.of<AuthProvider>(context, listen: false);

    return Container(
      decoration: const BoxDecoration(
          color: Colors.blueGrey,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10))),
      child: ListTile(
        leading: Image.asset(
          "assets/images/logo_only.png",
          fit: BoxFit.fitWidth,
          width: 80,
        ),
        trailing: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              userLogin?.name ?? '',
              style: const TextStyle(color: Colors.white),
            ),
            IconButton(
              onPressed: () async {
                await _authState.logout();
                Navigator.pushReplacementNamed(context, '/');
              },
              color: Colors.yellow,
              tooltip: "Logouts",
              icon: const Icon(Icons.logout_rounded),
            ),
          ],
        ),
      ),
    );
  }
}
