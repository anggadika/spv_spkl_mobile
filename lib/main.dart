import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spv_spkl_mobile_clonning/common/app_route.dart';
import 'package:spv_spkl_mobile_clonning/providers/auth_provider.dart';
import 'package:spv_spkl_mobile_clonning/providers/head_provider.dart';
import 'package:spv_spkl_mobile_clonning/providers/line_provider.dart';

import 'common/app_theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => AuthProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => HeadProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => LineProvider(),
        ),
      ],
      builder: (context, child) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "SPKL | SPVMB",
        theme: appTheme,
        initialRoute: "/",
        onGenerateRoute: AppRoute.generateRoute,
      ),
    );
  }
}
