import 'dart:convert';

List<WorkerModel> workerModelFromJson(String str) => List<WorkerModel>.from(
    json.decode(str).map((x) => WorkerModel.fromJson(x)));

String workerModelToJson(List<WorkerModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

WorkerModel userLoginFromJson(String str) =>
    WorkerModel.fromJson(json.decode(str));

class WorkerModel {
  WorkerModel({
    this.departement,
    this.departementRecId,
    this.lob,
    this.name,
    this.personnelNumber,
    this.recId,
  });

  String? departement;
  int? departementRecId;
  String? lob;
  String? name;
  String? personnelNumber;
  int? recId;

  factory WorkerModel.fromJson(Map<String, dynamic> json) => WorkerModel(
        departement: json["departement"],
        departementRecId: json["departementRecId"],
        lob: json["lob"],
        name: json["name"],
        personnelNumber: json["personnelNumber"],
        recId: json["recId"],
      );

  Map<String, dynamic> toJson() => {
        "departement": departement,
        "departementRecId": departementRecId,
        "lob": lob,
        "name": name,
        "personnelNumber": personnelNumber,
        "recId": recId,
      };
}
