import 'dart:convert';

List<HeadModel> headModelFromJson(String str) =>
    List<HeadModel>.from(json.decode(str).map((x) => HeadModel.fromJson(x)));

String headModelToJson(List<HeadModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class HeadModel {
  HeadModel({
    this.approvalStatus,
    this.creator,
    this.departement,
    this.departementId,
    this.description,
    this.lob,
    this.recId,
    this.transDate,
  });

  String? approvalStatus;
  String? creator;
  String? departement;
  int? departementId;
  String? description;
  String? lob;
  int? recId;
  String? transDate;

  factory HeadModel.fromJson(Map<String, dynamic> json) => HeadModel(
        approvalStatus: json["approvalStatus"],
        creator: json["creator"],
        departement: json["departement"],
        departementId: json["departementId"],
        description: json["description"],
        lob: json["lob"],
        recId: json["recId"],
        transDate: json["transDate"],
      );

  Map<String, dynamic> toJson() => {
        "approvalStatus": approvalStatus,
        "creator": creator,
        "departement": departement,
        "departementId": departementId,
        "description": description,
        "lob": lob,
        "recId": recId,
        "transDate": transDate,
      };
}
