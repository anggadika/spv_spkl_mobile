import 'dart:convert';

List<LineModel> lineModelFromJson(String str) =>
    List<LineModel>.from(json.decode(str).map((x) => LineModel.fromJson(x)));

String lineModelToJson(List<LineModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class LineModel {
  LineModel({
    this.approvalStatus,
    this.departement,
    this.diAwal,
    this.fromTime,
    this.hasilLembur,
    this.headRecId,
    this.note,
    this.position,
    this.recId,
    this.targetLembur,
    this.toTime,
    this.totalJamLembur,
    this.transDate,
    this.uangMakan,
    // this.update,
    this.workerId,
    this.workerName,
  });

  String? approvalStatus;
  String? departement;
  int? diAwal;
  String? fromTime;
  String? hasilLembur;
  int? headRecId;
  String? note;
  String? position;
  int? recId;
  String? targetLembur;
  String? toTime;
  String? totalJamLembur;
  String? transDate;
  int? uangMakan;
  // bool? update;
  int? workerId;
  String? workerName;

  factory LineModel.fromJson(Map<String, dynamic> json) => LineModel(
        approvalStatus: json["approvalStatus"],
        departement: json["departement"],
        diAwal: json["diAwal"],
        fromTime: json["fromTime"],
        hasilLembur: json["hasilLembur"],
        headRecId: json["headRecId"],
        note: json["note"],
        position: json["position"],
        recId: json["recId"],
        targetLembur: json["targetLembur"],
        toTime: json["toTime"],
        totalJamLembur: json["totalJamLembur"],
        transDate: json["transDate"],
        uangMakan: json["uangMakan"],
        // update: json["update"],
        workerId: json["workerId"],
        workerName: json["workerName"],
      );

  Map<String, dynamic> toJson() => {
        "approvalStatus": approvalStatus,
        "departement": departement,
        "diAwal": diAwal,
        "fromTime": fromTime,
        "hasilLembur": hasilLembur,
        "headRecId": headRecId,
        "note": note,
        "position": position,
        "recId": recId,
        "targetLembur": targetLembur,
        "toTime": toTime,
        "totalJamLembur": totalJamLembur,
        "transDate": transDate,
        "uangMakan": uangMakan,
        // "update": update,
        "workerId": workerId,
        "workerName": workerName,
      };
}
