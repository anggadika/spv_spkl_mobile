import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:spv_spkl_mobile_clonning/common/api_connect.dart';
import 'package:spv_spkl_mobile_clonning/common/app_global.dart';
import 'package:spv_spkl_mobile_clonning/models/worker_model.dart';
import 'package:spv_spkl_mobile_clonning/providers/init_provider.dart';

class AuthProvider extends InitProvider {
  List<WorkerModel> dataList = [];

  Future<WorkerModel?> checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userLogin = userLoginFromJson(prefs.getString('user') ?? '');
    dataList.clear();

    return userLogin;
  }

  Future<String> login(String username, String password) async {
    String _result = "";
    setLoadingState(true);

    final callApi = await ApiConnect()
        .getApi('/Login?username=$username&password=$password');

    if (callApi != "") {
      final bodyJson = json.decode(callApi);

      if (bodyJson['status'] == true) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("user", json.encode(bodyJson['data']));

        userLogin = userLoginFromJson(prefs.getString('user') ?? '');
      }

      _result = bodyJson['message'];
    }

    setLoadingState(false);

    return _result;
  }

  Future<List<WorkerModel>> fetchData() async {
    if (dataList.isEmpty) {
      final callApi = await ApiConnect()
          .getApi('/GetWorker?departement=${userLogin?.departementRecId}');

      if (callApi != "") {
        final bodyJson = json.decode(callApi);

        if (bodyJson['status'] == true) {
          dataList = workerModelFromJson(json.encode(bodyJson['data']));
        }
      }
    }
    return dataList;
  }

  Future logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('user');
    userLogin = null;
    notifyListeners();
  }
}
