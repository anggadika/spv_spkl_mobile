import 'dart:convert';
import 'package:spv_spkl_mobile_clonning/common/api_connect.dart';
import 'package:spv_spkl_mobile_clonning/common/app_global.dart';
import 'package:spv_spkl_mobile_clonning/models/head_model.dart';
import 'package:spv_spkl_mobile_clonning/providers/init_provider.dart';

class HeadProvider extends InitProvider {
  List<HeadModel> dataList = [];
  bool nextPage = true;
  bool prevPage = false;
  String searchText = "";
  int nextData = 0;
  int totalData = 0;

  Future<void> loadData() async {
    nextData = 0;
    dataList = [];
    nextPage = true;
    prevPage = false;
    await fetchData();
  }

  setNextPage() async {
    prevPage = true;
    await fetchData();
  }

  setPrevPage() async {
    nextPage = true;
    nextData = nextData == 21 ? nextData - 21 : nextData - 40;

    await fetchData();
  }

  Future<void> searchData(String searchValue) async {
    searchText = searchValue;
    nextData = 0;
    dataList = [];
    nextPage = true;
    prevPage = false;

    await fetchData();
  }

  Future<List<HeadModel>> fetchData() async {
    if (nextPage || prevPage) {
      final callApi = await ApiConnect().getApi(
          '/GetSPKL?departementId=${userLogin?.departementRecId}&name=$searchText&nextData=$nextData');

      if (callApi != "") {
        final bodyJson = json.decode(callApi);

        if (bodyJson['status'] == true) {
          dataList = headModelFromJson(json.encode(bodyJson['data']));

          nextData = bodyJson['rowStart'];
          totalData = nextData + dataList.length - 1;

          nextPage = dataList.length == 20 ? true : false;
          prevPage = totalData > 20 ? true : false;
        }
      }
    }

    notifyListeners();

    return dataList;
  }

  Future<String> added(String transDate, String description) async {
    String _result = "";

    if (userLogin?.lob == "") {
      _result = "Gagal, user login belum ada LOB.";
    } else {
      setLoadingState(true);

      final rawData = jsonEncode({
        "departementId": userLogin?.departementRecId,
        "transDate": transDate,
        "description": description,
        "creator": userLogin?.personnelNumber,
        "lob": userLogin?.lob
      });

      final callApi = await ApiConnect().postApi('/InsertSPKL', rawData);

      if (callApi != "") {
        final bodyJson = json.decode(callApi);
        _result = bodyJson;
      }

      setLoadingState(false);
    }

    return _result;
  }

  Future<HeadModel> update(
      HeadModel dataLast, String transDate, String description) async {
    setLoadingState(true);

    final rawData = jsonEncode({
      "recId": dataLast.recId,
      "transDate": transDate,
      "description": description,
      "lob": userLogin?.lob
    });

    final callApi = await ApiConnect().postApi('/UpdateSPKL', rawData);

    if (callApi != "") {
      final bodyJson = json.decode(callApi);
      if (bodyJson == 'success') {
        dataLast.transDate = transDate;
        dataLast.description = description;
      }
    }

    setLoadingState(false);

    return dataLast;
  }

  Future<String> action(String method, int? recId) async {
    String _result = "";

    setLoadingState(true);

    final callApi = await ApiConnect().getApi('$method?headId=$recId');

    if (callApi != "") {
      final bodyJson = json.decode(callApi);
      _result = bodyJson;
      if (_result == 'success') {
        await loadData();
      } else {
        MyAlert().snackBar(ctxGlobal!, _result);
      }
    }

    setLoadingState(false);

    return _result;
  }
}
