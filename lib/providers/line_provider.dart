import 'dart:convert';
import 'package:spv_spkl_mobile_clonning/models/head_model.dart';
import 'package:spv_spkl_mobile_clonning/models/line_model.dart';
import 'package:spv_spkl_mobile_clonning/models/worker_model.dart';
import 'package:spv_spkl_mobile_clonning/providers/init_provider.dart';
import 'package:spv_spkl_mobile_clonning/common/api_connect.dart';

class LineProvider extends InitProvider {
  List<LineModel> dataList = [];
  WorkerModel? workerPick;
  HeadModel headData = HeadModel();

  setHeadData(HeadModel val) {
    headData = val;
    notifyListeners();
  }

  Future<List<LineModel>> fetchData() async {
    final callApi =
        await ApiConnect().getApi('/GetSPKLLine?headId=${headData.recId}');

    if (callApi != "") {
      final bodyJson = json.decode(callApi);

      if (bodyJson['status'] == true) {
        dataList = lineModelFromJson(json.encode(bodyJson['data']));
      }
    }
    notifyListeners();
    return dataList;
  }

  Future<String> added(String mulai, String selesai, String note, String target,
      String hasil) async {
    String _result = "";

    if (workerPick == null) {
      _result = "Worker harus dipilih.";
    } else if (mulai == selesai) {
      _result = "Jam mulai dan selesai tidak boleh sama.";
    } else {
      setLoadingState(true);

      final rawData = jsonEncode({
        "headRecId": headData.recId,
        "workerId": workerPick?.recId,
        "fromTime": mulai,
        "toTime": selesai,
        "diAwal": 0,
        "note": note,
        "targetLembur": target,
        "hasilLembur": hasil,
        "update": false
      });

      final callApi = await ApiConnect().postApi('/InsertSPKLLine', rawData);

      if (callApi != "") {
        final bodyJson = json.decode(callApi);
        _result = bodyJson;
      }

      setLoadingState(false);
    }

    return _result;
  }

  Future<String> edited(String mulai, String selesai, String note,
      String target, String hasil) async {
    String _result = "";

    if (workerPick == null) {
      _result = "Worker harus dipilih.";
    } else if (mulai == selesai) {
      _result = "Jam mulai dan selesai tidak boleh sama.";
    } else {
      setLoadingState(true);

      final rawData = jsonEncode({
        "headRecId": headData.recId,
        "workerId": workerPick?.recId,
        "fromTime": mulai,
        "toTime": selesai,
        "diAwal": 0,
        "note": note,
        "targetLembur": target,
        "hasilLembur": hasil,
        "update": true
      });

      final callApi = await ApiConnect().postApi('/UpdateSPKLLine', rawData);

      if (callApi != "") {
        final bodyJson = json.decode(callApi);
        _result = bodyJson;
      }

      setLoadingState(false);
    }

    return _result;
  }

  Future<String> delete(int? recId) async {
    String _result = "";

    setLoadingState(true);

    final callApi = await ApiConnect().getApi('/DeleteLine?lineId=$recId');

    if (callApi != "") {
      final bodyJson = json.decode(callApi);
      _result = bodyJson;

      await fetchData();
    }

    setLoadingState(false);

    return _result;
  }
}
