import 'package:flutter/foundation.dart';

class InitProvider extends ChangeNotifier {
  bool _loadingState = false;

  bool get loadingState => _loadingState;

  setLoadingState(bool val) {
    _loadingState = val;
    notifyListeners();
  }
}
